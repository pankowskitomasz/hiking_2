import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import OffersS1 from "../components/offers-s1";
import OffersS2 from "../components/offers-s2";
import OffersS3 from "../components/offers-s3";

class Offers extends Component{
    render(){
        return(        
            <Container fluid className="minh-100vh p-0">
                <OffersS1/>
                <OffersS2/>
                <OffersS3/>
            </Container>    
        );
    }
}

export default Offers;